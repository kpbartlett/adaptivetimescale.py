#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  9 16:24:11 2018

Scale for datetime plots.

To see description of program, type
  import AdaptiveTimeScale
  help(AdaptiveTimeScale.AdaptiveTimeScale)

To run in demo mode, type
  python AdaptiveTimeScale.py

@author: Kevin Bartlett. kevin.bartlett.175@gmail.com.
"""

"""
REVISION HISTORY:

2018-08-02--Added workarounds to strftime failing for years < 1900.


"""

import numpy as np
from matplotlib.ticker import FuncFormatter
from matplotlib.ticker import MultipleLocator
from matplotlib import scale as mscale
import datetime as dt
import matplotlib.dates as mdate
from dateutil.relativedelta import relativedelta

# Codes for time units are chosen such that small values correspond to large
# time units. This is used later in the code, so don't change them.
timeUnitCodes = {'YEARS':1,'MONTHS':2,'DAYS':3,'HOURS':4,'MINUTES':5,'SECONDS':6,'MILLISECONDS':7}

# Because of way that calc_tick_values_and_labels() calculates ticks,
# the maximum time value supported by AdaptiveTimeScale is one year
# LESS than the maximum time value supported by datetime.
maxTime = mdate.date2num((dt.datetime.max - relativedelta(days=366)))

class AdaptiveTimeScaleError(Exception):
   """Basic exception for AdaptiveTimeScale"""

class RangeTooSmallError(AdaptiveTimeScaleError):
   """Raised when the range of tlims is too small"""

class TlimsNotListError(AdaptiveTimeScaleError):
   """Raised when tlims is not a list"""

class TlimsNotDatetimesError(AdaptiveTimeScaleError):
   """Raised when elements of tlims are not datetime.datetime objects"""

class TlimsNotIncreasingError(AdaptiveTimeScaleError):
   """Raised when tlims is not increasing"""

class TlimsTooSmallError(AdaptiveTimeScaleError):
   """Raised when elements of tlims smaller than minimum allowed value"""

class TlimsTooLargeError(AdaptiveTimeScaleError):
   """Raised when elements of tlims exceed maximum allowed value"""

class AdaptiveTimeScale(mscale.ScaleBase):
    """
    Scale for datetime plots.

    Modelled after LogScale in matplotlib.scale.py.

    Limited to time between the years 0 and 9998.

    Call in demo mode:
      python AdaptiveTimeScale.py

    Example of use:
      import AdaptiveTimeScale
      import datetime as dt
      import numpy as np
      import matplotlib.pyplot as plt
      y = np.array([440, 100, 200, 50, 400, 300, 60, 70, 80, 90])
      timeDelta = dt.timedelta(days=1)
      t0 = dt.datetime(2015, 12, 31, 16, 0, 0, 0)
      t = [t0 + timeDelta * j for j in range(len(y))]
      tlims = [np.min(t),np.max(t)]
      fig = plt.figure()
      ax1 = fig.add_subplot(2,1,1)
      ax1.plot(t, y,'o-')
      ax1.set_xlim([tlims[0],tlims[1]])
      ax1.set_xscale('adaptivetime',doLabelTicks=False,doLabelAx=False,maxTicks=10)
      ax1.set_ylabel('y')
      ax1.set_title('Adaptive Time Scale')
      ax2 = fig.add_subplot(2,1,2)
      ax2.plot(t, y,'o-')
      ax2.set_xlim([tlims[0],tlims[1]])
      ax2.set_xscale('adaptivetime',timeZoneStr="UTC",maxTicks=10,CustomModule="my_custom_formats_french",someOtherArg=99)
      ax2.set_ylabel('y')
      plt.show()

    The example above includes the use of several keyword arguments:

      "doLabelAx" set to False
         ==> axis labelling suppressed in upper plot;

      "doLabelTicks" set to False
         ==> axis labelling suppressed in upper plot;

      "maxTicks" set to 10
         ==> prevents thinning of ticks by allowing more than the default
             maximum number.

      "timeZoneStr" set to "UTC"
         ==> the timezone string "UTC" (padded with a leading space) is
             appended to the axis label.

      "CustomModule" set to "my_custom_formats_french"
         ==> One or both of the functions generate_ticklabel() and
             generate_axlabel() are redefined from the default versions to the
             versions defined in the file called my_custom_formats_french.py.
             This allows the user to define custom tick and axis label formats
             (as for internationalization) without modifying the original code.
             In this example, French-language labels would be generated.

      "someOtherArg" set to 99
         ==> The keyword/value pair "someOtherArg"/99 is passed to the
             generate_ticklabel() and generate_axlabel() functions. This is to
             facilitate customization. Note that "someOtherArg" is just an
             arbitrary example; any argument name can be used.

    See
      help(AdaptiveTimeScale.calc_ticks)
    for descriptions of keyword arguments that are used in calculating tick
    positions and labels.

    """
    name = 'adaptivetime'

    def __init__(self, axis, **kwargs):
        self.doLabelTicks = kwargs.pop('doLabelTicks',True)
        self.doLabelAx = kwargs.pop('doLabelAx',True)
        self.timeZoneStr = kwargs.pop('timeZoneStr',"")
        CustomModule = kwargs.pop('CustomModule',None)
        self.kwargs = kwargs
        self.maxTicks = kwargs.pop('maxTicks',None)
        self.maxTicksAdjust = kwargs.pop('maxTicksAdjust',0)
        self.timeUnitCodes = timeUnitCodes
        self.tickVals = []
        self.tickLabelStrs = {}
        self.tickTypes = {}
        self.axLabelStr = ""
        self.tlims = None
        self.axLabelUnits = None
        self.numInLabel = None
        self.axis = axis

        # Incorporate any extra arguments into object for easy passing to custom format module.
        self.extraArgs = kwargs

        if CustomModule:
           try:
               self.axLabelFunction = __import__(CustomModule).generate_axlabel
           except AttributeError:
               self.axLabelFunction = generate_axlabel

           try:
               self.tickLabelFunction = __import__(CustomModule).generate_ticklabel
           except AttributeError:
               self.tickLabelFunction = generate_ticklabel

        else:
           self.axLabelFunction = generate_axlabel
           self.tickLabelFunction = generate_ticklabel

    def set_default_locators_and_formatters(self, axis):
        """
        Set the locators and formatters to specialized versions.
        """
        locator = AdaptiveTimeTickLocator(self)
        axis.set_major_locator(locator)
        #formatter = AdaptiveTimeTickFormatter(locator,self.doLabelTicks)
        formatter = AdaptiveTimeTickFormatter(self,locator)
        axis.set_major_formatter(formatter)
        axis.set_minor_locator(mscale.NullLocator())
        axis.set_minor_formatter(mscale.NullFormatter())

    def get_transform(self):
        """
        Return a :class:`~matplotlib.transforms.Transform` instance.
        """
        # Do no transformation.
        return mscale.IdentityTransform()

    def limit_range_for_scale(self, vmin, vmax, minpos):
        """
        Limit the domain to valid range of datetime values.
        """

        # Limit domain to greater than or equal to minimum value supported by
        # datetime (datetime.datetime(1, 1, 1, 0, 0)).
        if vmin <= mdate.date2num(dt.datetime.min):
            #vmin2 = minpos
            vmin2 = mdate.date2num(dt.datetime.min)
        else:
            vmin2 = vmin

        if vmax <= mdate.date2num(dt.datetime.min):
            #vmax2 = minpos
            vmax2 = mdate.date2num(dt.datetime.min + dt.timedelta(seconds=1))
        else:
            vmax2 = vmax

        if vmin2 >= maxTime:
            #vmin2 = mdate.date2num(dt.datetime.max - relativedelta(days=366) - relativedelta(seconds=1))
            vmin2 = maxTime - relativedelta(seconds=1)

        if vmax2 >= maxTime:
            vmax2 = maxTime

        return (vmin2,vmax2)

class AdaptiveTimeTickLocator(MultipleLocator):
  def __init__(self,scale):
      self.scale = scale

  def tick_values(self, a, b):
    # Method inherited from class MultipleLocator. Takes axis limits min,max
    # as input arguments. Needs to return tick values.
    # e.g., return (tickVal1,tickVal3,tickVal2)

    # 2018-07-13, kpb--This fails if initial x-limits are outside correct
    # range. Add test.
    try:
        tlims = mdate.num2date([a,b])
        goodLims = True
    except ValueError:
        goodLims = False

    if not goodLims:
        haveTicks = False
    else:
        try:
          tickVals,tickTypes,axLabelUnits, numInLabel = calc_ticks(tlims, self.scale.maxTicks, self.scale.maxTicksAdjust)
          haveTicks = True
        except RangeTooSmallError as err:
          haveTicks = False

    if haveTicks:
        # Each time new tick values are calculated, create new tick label strings
        # and a new axis label string to go with them.
        tickLabelStrs = dict(zip(tickVals,""*len(tickVals)))

        for currTickVal in tickTypes.keys():
            tickLabelStrs[currTickVal] = self.scale.tickLabelFunction(self.scale, currTickVal,tickTypes)
        axLabelStr = self.scale.axLabelFunction(self.scale,tlims,axLabelUnits,numInLabel,self.scale.timeZoneStr)
    else:
        tickVals = []
        tickTypes = {}
        axLabelUnits = None
        numInLabel = {}
        tickLabelStrs = {}
        axLabelStr = ''

    self.scale.tickLabelStrs = tickLabelStrs

    # Apply the axis label now.
    if self.scale.doLabelAx:
       self.scale.axis.set_label_text(axLabelStr)
    else:
       self.scale.axis.set_label_text("")

    return tickVals

class AdaptiveTimeTickFormatter(FuncFormatter):
  def __init__(self,scale,tickLocator):
    super(AdaptiveTimeTickFormatter,self).__init__(self.formatter)
    self.tickLocator = tickLocator
    self.scale = scale

  def formatter(self,tickVal, pos):
    if tickVal in self.scale.tickLabelStrs.keys():
        if self.scale.doLabelTicks:
           tickStr = self.scale.tickLabelStrs[tickVal]
        else:
           tickStr = ""
    else:
        # This string is used by the matplotlib mouse-hover coordinate display;
        # should display full yyyy-mm-dd HH:MM:SS.fff format.

        # 2018-08-02-Workaround for fact that strftime('%Y') fails for years < 1900. Extract a year string
        # then "correct" the year value to a value >= 1900.
        #tickStr = mdate.num2date(tickVal).strftime('%Y-%m-%d %H:%M:%S.%f')
        yearVal = mdate.num2date(tickVal).year
        yearStr = "%04d" % yearVal
        correctedTickVal = mdate.num2date(tickVal)
        if yearVal < 1900:
           correctedTickVal = dt.datetime(1901,correctedTickVal.month,correctedTickVal.day,correctedTickVal.hour,correctedTickVal.minute,correctedTickVal.second,correctedTickVal.microsecond)
        tickStr = yearStr + correctedTickVal.strftime('-%m-%d %H:%M:%S.%f')
    return tickStr

def calc_ticks(tlims, maxTicks, maxTicksAdjust):
  """
  Calculates the ticks, tick labels, and axis label suitable for the given
  time limits.

  Syntax: calculatedTicks, axLabelStr = calc_tick_values_and_labels(tlims, maxTicks, maxTicksAdjust)

  Input arguments:

      tlims: 1x2 list of datetime objects.

      maxTicks: The function will thin the ticks if there are more than this
        many. The default value (provided as constants below) is different for
        different time-tick units.

      maxTicksAdjust: Adjust the maximum number of ticks by this amount
        (positive or negative integer) from the default number. Has no effect
        if a value for maxTicks has been specified.

  """

  # Constants:

  # Tick labels for some time-tick units take up more room than others. For
  # example, seconds (really HH:MM:SS) takes up more space than hours or
  # minutes (both really HH:MM). For this reason, it may be useful to be able
  # to adjust the values of the constants giving the default maximum number of ticks
  # separately for the different time-tick units.
  YEARS_MAXTICKS = 8
  MONTHS_MAXTICKS = 8
  DAYS_MAXTICKS = 8
  HOURS_MAXTICKS = 8
  MINUTES_MAXTICKS = 8
  SECONDS_MAXTICKS = 6
  MILLISECONDS_MAXTICKS = 8

  if not isinstance(tlims,list):
      raise TlimsNotListError("tlims should be a list.")

  if not (isinstance(tlims[0],dt.datetime) and isinstance(tlims[1],dt.datetime)):
      raise TlimsNotDatetimesError("tlims should be a list of datetime.datetime objects.")

  if tlims[0] >= tlims[1]:
      raise TlimsNotIncreasingError("tlims[0] should be less than tlims[1].")

  # Use "naive" (non-timezone aware) times to allow comparison with datetime min and max.
  tlims[0] = tlims[0].replace(tzinfo=None)
  tlims[1] = tlims[1].replace(tzinfo=None)

  if not np.all(np.array(tlims) >= dt.datetime.min):
      raise TlimsTooSmallError("tlims values must be greater than or equal to datetime.datetime.min.")

  if not np.all(np.array(tlims) <= mdate.num2date(maxTime).replace(tzinfo=None)):
      raise TlimsTooLargeError("tlims values must be less than " + mdate.num2date(maxTime).strftime('%Y-%m-%d %H:%M:%S') + ".")

  tzinfo = tlims[0].tzinfo

  if maxTicks != None:
    # Any value specified for maxTicksAdjust has no effect if maxTicks
    # has been specified.
    maxTicksAdjust = 0

  # Tick values are calculated starting with the largest time units (years)
  # and progressing through months, days, hours, minutes and seconds until
  # the maximum desired number of ticks is reached or exceeded. The point of this
  # is to avoid calculating enormously long vectors of minutes and seconds
  # when dealing with decade-long data sets, for example.
  #
  # The resulting set of tick values serves as a starting point for
  # determining what ticks to place on the axes--time units are chosen such
  # that maxTicks or more ticks will be created, and the ticks will be thinned, if
  # necessary, until no more than maxTicks ticks remain.
  possibleMaxTicks = (YEARS_MAXTICKS, MONTHS_MAXTICKS, DAYS_MAXTICKS, HOURS_MAXTICKS, MINUTES_MAXTICKS, SECONDS_MAXTICKS, MILLISECONDS_MAXTICKS)
  maxMaxTicks = np.max(possibleMaxTicks) + maxTicksAdjust
  if maxTicks != None:
    maxMaxTicks = np.max([maxMaxTicks,maxTicks])

  # Choose the time units to calibrate the time axes by.

  # ...Find the first possible tick for each of the defined time units by rounding
  # down to integer values.
  year1,month1,day1,hour1,minute1,second1,dummy,dummy,dummy = tlims[0].timetuple()
  # ...Round microseconds down to nearest millisecond integer.
  millisecond1 = int(round((tlims[0].microsecond/1000.)))
  microsecond1 = millisecond1 * 1000
  if microsecond1 > 999999:
      #microsecond1 = 999999
      microsecond1 = 999000

  FirstMilliSecondTick = dt.datetime(year1,month1,day1,hour1,minute1,second1,microsecond1, tzinfo)

  if FirstMilliSecondTick < tlims[0]:
    FirstMilliSecondTick = FirstMilliSecondTick + dt.timedelta(milliseconds=1)
  FirstSecondTick = dt.datetime(year1,month1,day1,hour1,minute1,second1,0, tzinfo)
  if FirstSecondTick < tlims[0]:
    FirstSecondTick = FirstSecondTick + dt.timedelta(seconds=1)
  FirstMinuteTick = dt.datetime(year1,month1,day1,hour1,minute1,0,0, tzinfo)
  if FirstMinuteTick < tlims[0]:
    FirstMinuteTick = FirstMinuteTick + dt.timedelta(minutes=1)
  FirstHourTick = dt.datetime(year1,month1,day1,hour1,0,0,0, tzinfo)
  if FirstHourTick < tlims[0]:
    FirstHourTick = FirstHourTick + dt.timedelta(hours=1)
  FirstDayTick = dt.datetime(year1,month1,day1,0,0,0,0, tzinfo)
  if FirstDayTick < tlims[0]:
    FirstDayTick = FirstDayTick + dt.timedelta(days=1)
  FirstMonthTick = dt.datetime(year1,month1,1,0,0,0,0, tzinfo)
  if FirstMonthTick < tlims[0]:
    # "months" is not a valid argument for timedelta. Need to add a month a different way.
    if month1 == 12:
      FirstMonthTick = dt.datetime(year1+1,1,1,0,0,0,0, tzinfo)

  FirstYearTick = dt.datetime(year1,1,1,0,0,0,0, tzinfo)
  if FirstYearTick < tlims[0]:
    FirstYearTick = dt.datetime(year1+1,1,1,0,0,0,0, tzinfo)

  # ...Find the last possible tick for each of the defined time units.
  year2 = tlims[1].year
  month2 = tlims[1].month
  day2 = tlims[1].day
  hour2 = tlims[1].hour
  minute2 = tlims[1].minute
  second2 = tlims[1].second
  microsecond2 = tlims[1].microsecond
  LastMilliSecondTick = dt.datetime(year2,month2,day2,hour2,minute2,second2,int(np.floor((microsecond2/1000)*1000)), tzinfo)
  LastSecondTick = dt.datetime(year2,month2,day2,hour2,minute2,int(np.floor(second2)),0, tzinfo)
  LastMinuteTick = dt.datetime(year2,month2,day2,hour2,minute2,0,0, tzinfo)
  LastHourTick = dt.datetime(year2,month2,day2,hour2,0,0,0, tzinfo)
  LastDayTick = dt.datetime(year2,month2,day2,0,0,0,0, tzinfo)
  LastMonthTick = dt.datetime(year2,month2,1,0,0,0,0, tzinfo)
  LastYearTick = dt.datetime(year2,1,1,0,0,0,0, tzinfo)

  # If there is no year tick available between the first and second time
  # limit, then the first possible year tick (calculated above) will be
  # greater than the last possible year tick. Correct for these cases.
  if FirstYearTick > LastYearTick:
      FirstYearTick = None;
      LastYearTick = None;

  # Similarly for months, days, hours, minutes, seconds, milliseconds.
  if FirstMonthTick > LastMonthTick:
      FirstMonthTick = None;
      LastMonthTick = None;

  if FirstDayTick > LastDayTick:
      FirstDayTick = None;
      LastDayTick = None;

  if FirstHourTick > LastHourTick:
      FirstHourTick = None;
      LastHourTick = None;

  if FirstMinuteTick > LastMinuteTick:
      FirstMinuteTick = None;
      LastMinuteTick = None;

  if FirstSecondTick > LastSecondTick:
      FirstSecondTick = None;
      LastSecondTick = None;

  if FirstMilliSecondTick > LastMilliSecondTick:
      raise RangeTooSmallError("Time range too small to convert to time strings.")

  # Find the tick values.
  MonthTicks = []
  DayTicks = []
  HourTicks = []
  MinuteTicks = []
  SecondTicks = []
  MilliSecondTicks = []

  # ...Get the year tick values (Needed for all time units).
  if year2>year1 and (not FirstYearTick==None) and (not LastYearTick==None):
    FirstVal = FirstYearTick.year
    LastVal = LastYearTick.year
    YearVector = np.array(range(FirstVal,LastVal+1,1))
    YearTicks = [dt.datetime(i,1,1,0,0,0,0, tzinfo) for i in YearVector]
  else:
      YearVector = year1
      YearTicks = []

  BaseUnits = timeUnitCodes['YEARS']
  NumYearTicks   = len(YearTicks)
  NumMonthTicks  = 0
  NumDayTicks    = 0
  NumHourTicks   = 0
  NumMinuteTicks = 0
  NumSecondTicks = 0
  NumMilliSecondTicks = 0

  #################################################################################################
  # If the the number of year ticks does not satisfy the requirement for a
  # minimum number of ticks, go ahead and get the month tick values.
  if NumYearTicks < maxMaxTicks:
      # Create a vector of dates marking the start of months. This vector is
      # based on a standard month length and may not coincide with actual
      # month ticks.
      if FirstMonthTick == None:
          MonthTicks = []
      else:
          ApproxNumMonthTicks = int(np.ceil( (LastMonthTick - FirstMonthTick).total_seconds()/((365.25*24*3600)/12) ) + 2)

          # Create a vector of integer month numbers. Values may exceed the number of months in a year.
          IntMonths = [FirstMonthTick.month + i for i in range(ApproxNumMonthTicks)]

          # "Wrap" months to run from 1 to 12. Adjust years for end-of-years if detected.
          FirstMonthTickYear = FirstMonthTick.year
          IntYears = [FirstMonthTickYear] * len(IntMonths)

          for MonthCount in range(len(IntMonths)):
              if IntMonths[MonthCount]>12:
                # Wrap this month and all subsequent months by subtracting 12.
                for ind in range(MonthCount,len(IntMonths),1):
                  IntMonths[ind] = IntMonths[ind] - 12
                  IntYears[ind] = IntYears[ind] + 1

          MonthTicks = []
          if len(IntMonths)>0:
            for ind in range(0,len(IntMonths),1):
              MonthTicks.append(dt.datetime(IntYears[ind],IntMonths[ind],1,0,0,0,0, tzinfo))

          # Remove any ticks that exceed the time limits of the plot.
          MonthTicks = np.array(MonthTicks)
          #MonthTicks = list(MonthTicks[MonthTicks>=tlims[0] and MonthTicks<=tlims[1]])
          ind = np.nonzero(np.logical_and(MonthTicks>=tlims[0], MonthTicks<=tlims[1]))
          MonthTicks = MonthTicks[ind]
      NumMonthTicks = len(MonthTicks)

      # ...If there are not sufficient month ticks, get the day tick values.
      if NumMonthTicks < maxMaxTicks:
          if FirstDayTick == None:
              DayTicks = []
          else:
              ApproxNumDayTicks = int(np.ceil( (LastDayTick - FirstDayTick).total_seconds()/(24*3600.) )) + 2
              DayTicks = [FirstDayTick + dt.timedelta(days=1)*i for i in np.array(range(0,ApproxNumDayTicks,1))]

              # Remove any ticks that exceed the time limits of the plot.
              DayTicks = np.array(DayTicks)
              DayTicks = list(DayTicks[DayTicks<=tlims[1]])
          NumDayTicks = len(DayTicks)

          # If there are not sufficient day ticks, get the hour tick values.
          if NumDayTicks < maxMaxTicks:

              if FirstHourTick == None:
                  HourTicks = []
              else:
                  ApproxNumHourTicks = int(np.ceil( (LastHourTick - FirstHourTick).total_seconds()/(60.0*60.0) )) + 2
                  HourTicks = [FirstHourTick + dt.timedelta(hours=1)*i for i in np.array(range(0,ApproxNumHourTicks,1))]

                  # Remove any ticks that exceed the time limits of the plot.
                  HourTicks = np.array(HourTicks)
                  #HourTicks = list(HourTicks[HourTicks<=tlims[1]])
                  ind = np.nonzero(np.logical_and(HourTicks>=tlims[0], HourTicks<=tlims[1]))
                  HourTicks = HourTicks[ind]
              NumHourTicks = len(HourTicks)

              # If there are not sufficient hour ticks, get the minute tick values.
              if NumHourTicks < maxMaxTicks:
                  if FirstMinuteTick == None:
                      MinuteTicks = []
                  else:
                      ApproxNumMinuteTicks = int(np.ceil( (LastMinuteTick - FirstMinuteTick).total_seconds()/(60.) )) + 2
                      MinuteTicks = [FirstMinuteTick + dt.timedelta(minutes=1)*i for i in np.array(range(0,ApproxNumMinuteTicks,1))]

                      # Remove any ticks that exceed the time limits of the plot.
                      MinuteTicks = np.array(MinuteTicks)
                      #MinuteTicks = list(MinuteTicks[MinuteTicks<=tlims[1]])
                      ind = np.nonzero(np.logical_and(MinuteTicks>=tlims[0], MinuteTicks<=tlims[1]))
                      MinuteTicks = MinuteTicks[ind]
                  NumMinuteTicks = len(MinuteTicks)
                  # If there are not sufficient minute ticks, get the second tick values.
                  if NumMinuteTicks < maxMaxTicks:
                    if FirstSecondTick == None:
                       SecondTicks = []
                    else:
                       ApproxNumSecondTicks = int(np.ceil( (LastSecondTick - FirstSecondTick).total_seconds() )) + 2
                       SecondTicks = [FirstSecondTick + dt.timedelta(seconds=1)*i for i in np.array(range(0,ApproxNumSecondTicks,1))]

                    # Remove any ticks that exceed the time limits of the plot.
                    SecondTicks = np.array(SecondTicks)
                    ind = np.nonzero(np.logical_and(SecondTicks>=tlims[0], SecondTicks<=tlims[1]))
                    SecondTicks = SecondTicks[ind]
                    NumSecondTicks = len(SecondTicks)

                    # If there are not sufficient seconds ticks, get the millisecond tick values.
                    if NumSecondTicks < maxMaxTicks:
                      ApproxNumMilliSecondTicks = int(np.ceil( (LastMilliSecondTick - FirstMilliSecondTick).total_seconds()*1000 )) + 2
                      MilliSecondTicks = [FirstMilliSecondTick + dt.timedelta(milliseconds=1)*i for i in np.array(range(0,ApproxNumMilliSecondTicks,1))]

                      # Remove any ticks that exceed the time limits of the plot.
                      MilliSecondTicks = np.array(MilliSecondTicks)
                      ind = np.nonzero(np.logical_and(MilliSecondTicks>=tlims[0], MilliSecondTicks<=tlims[1]))
                      MilliSecondTicks = MilliSecondTicks[ind]
                      NumMilliSecondTicks = len(MilliSecondTicks)

  # Find the largest time unit for which the number of available ticks
  # exceeds the desired minimum number of ticks. This will be the time
  # unit by which the time axis will be delineated.
  numAvailTicks = (NumYearTicks, NumMonthTicks, NumDayTicks, NumHourTicks, NumMinuteTicks, NumSecondTicks, NumMilliSecondTicks)
  availTickUnits = (timeUnitCodes['YEARS'], timeUnitCodes['MONTHS'], timeUnitCodes['DAYS'], timeUnitCodes['HOURS'], timeUnitCodes['MINUTES'], timeUnitCodes['SECONDS'], timeUnitCodes['MILLISECONDS'])
  ind = np.nonzero(np.array(numAvailTicks) >= maxMaxTicks)
  ind = ind[0]
  if len(ind) == 0:
    # There is no time unit for which the number of available ticks exceeds the
    # desired minimum number of ticks, so choose the smallest time unit as the
    # base time unit.
    BaseUnits = timeUnitCodes['MILLISECONDS']
  else:
    ind = ind[0]
    BaseUnits = availTickUnits[ind]

  # Certain variables will control how the time axis ticks will be placed and
  # labelled. Assign these variables according to which base time units
  # have been chosen:
  #   "MinorTicks" and "MajorTicks" give the tick locations as-is.
  #
  #   "TimeJumps" is a vector of permitted increments between ticks if
  #  they require thinning. TimeJumps is in units of the BaseUnits (e.g.,
  #  years, months, etc.), but because ticks are initially calculated
  #  sequentially, it can also work as an index to the unthinned ticks. The
  #  value of TimeJumps is never 1 because it is employed only when thinning
  #  is required.
  #
  #   "maxTicks" controls how many ticks can be plotted (different date label
  # formats require different amounts of space, so maxTicks varies from one
  # time unit to another).
  if BaseUnits == timeUnitCodes['YEARS']:
      MinorTicks = YearTicks
      MajorTicks = []
      MajorTicksUnits = None
      thisUnitsMaxTicks = YEARS_MAXTICKS

      # Allow time length to be open-ended by extending TimeJumps for years as
      # far as dictated by the time limits.
      BaseYearTimeJumps = [2, 2.5, 5, 10, 20, 25, 50, 100, 200, 250, 500, 1000]
      TimeJumps = [2, 5, 10, 20, 25, 50, 100, 200, 250, 500, 1000]
      BasePower = int(np.log10(np.max(BaseYearTimeJumps)))
      maxYear = tlims[1].year
      maxPower = int(BasePower*np.ceil(np.log10(maxYear)/BasePower))
      for thisPower in range(BasePower,maxPower+1,BasePower):
        TimeJumps.extend([int(BaseYearTimeJumps[i]*10**thisPower) for i in range(len(BaseYearTimeJumps))])
  elif BaseUnits == timeUnitCodes['MONTHS']:
      MinorTicks = MonthTicks
      MajorTicks = YearTicks
      MajorTicksUnits = timeUnitCodes['YEARS']
      thisUnitsMaxTicks = MONTHS_MAXTICKS
      TimeJumps = [2, 3, 4, 6, 12]
  elif BaseUnits == timeUnitCodes['DAYS']:
      MinorTicks = DayTicks
      MajorTicks = MonthTicks
      MajorTicksUnits = timeUnitCodes['MONTHS']
      thisUnitsMaxTicks = DAYS_MAXTICKS
      TimeJumps = [2, 5, 10, 50, 100]
  elif BaseUnits == timeUnitCodes['HOURS']:
      MinorTicks = HourTicks
      MajorTicks = DayTicks
      MajorTicksUnits = timeUnitCodes['DAYS']
      thisUnitsMaxTicks = HOURS_MAXTICKS
      TimeJumps = [2, 3, 4, 6, 8, 12, 24, 48]
  elif BaseUnits == timeUnitCodes['MINUTES']:
      MinorTicks = MinuteTicks
      MajorTicks = HourTicks
      MajorTicksUnits = timeUnitCodes['HOURS']
      thisUnitsMaxTicks = MINUTES_MAXTICKS
      TimeJumps = [2, 5, 10, 15, 30, 60, 120]
  elif BaseUnits == timeUnitCodes['SECONDS']:
      MinorTicks = SecondTicks
      MajorTicks = MinuteTicks
      MajorTicksUnits = timeUnitCodes['MINUTES']
      thisUnitsMaxTicks = SECONDS_MAXTICKS
      TimeJumps = [2, 5, 10, 15, 30, 60, 120]
  elif BaseUnits == timeUnitCodes['MILLISECONDS']:
      MinorTicks = MilliSecondTicks
      MajorTicks = SecondTicks
      MajorTicksUnits = timeUnitCodes['SECONDS']
      thisUnitsMaxTicks = MILLISECONDS_MAXTICKS
      TimeJumps = [2, 5, 10, 20, 25, 50, 100, 200, 250, 500, 1000]

  # Reverse dictionary for development only.
  #rev = {v: k for k, v in timeUnitCodes.items()}

  # If not overridden by user, the maximum number of ticks is chosen
  # automatically, according to what time units are in use.
  if maxTicks == None:
      maxTicks = thisUnitsMaxTicks + maxTicksAdjust

  # Want there always to be at least one tick.
  if maxTicks < 1:
     maxTicks = 1

  ################################################################
  # Thin the ticks if necessary.

  # Thin the number of ticks if there are too many to fit nicely on a plot
  # axis. Thin the ticks in such a way that the "major" ticks are not
  # skipped.
  if len(MinorTicks) <= maxTicks:
      tickVals = MinorTicks
  else:
      thinRatio = np.ceil(len(MinorTicks)/maxTicks)
      ind = np.nonzero(np.array(TimeJumps >= thinRatio))
      ind = ind[0] # Index to the smallest jump that will achieve the desired thinning.
      NiceTimeJump = TimeJumps[ind[0]]

      # The permitted time jumps have been chosen to ensure that the thinned minor
      # ticks will still include their associated major ticks. This doesn't work
      # for days, however, as months do not always contain the same number of days.
      # For this reason, a rather complicated algorithm that builds up the thinned
      # tick vector from the major ticks has to be used.
      if len(MajorTicks)>0:
        # There are major ticks. Fill in any minor ticks up to and including the first major tick.
        minorTickEqualToFirstMajorTickInd = np.nonzero(np.array(MinorTicks)==MajorTicks[0])[0][0]

        # Have to do range from AnchorIndex down to -1, not down to zero, because range does not include the endpoint in its output.
        ThinnedTickIndex = np.array(range(minorTickEqualToFirstMajorTickInd, -1, -NiceTimeJump))

        # Empty ThinnedTickIndex results in floats in results of union1d; have to convert back to ind.
        ThinnedTickIndex = [int(i) for i in np.union1d(ThinnedTickIndex,np.array([minorTickEqualToFirstMajorTickInd]))]

        # Fill in the minor ticks following each of the major ticks.
        majorCount = 0
        for MajorTick in MajorTicks:
          minorTickEqualToThisMajorTickInd = np.nonzero(np.array(MinorTicks)==MajorTick)[0][0]

          # Have to test for length minus one because of python's idiotic zero-based indexing.
          if majorCount < len(MajorTicks)-1:
            # There are more major ticks after this one, so fill in the
            # minor ticks only up to the next major tick.

            #minorTickEqualToNextMajorTickInd = np.nonzero(np.array(MinorTicks)==MajorTicks[0])[majorCount+1]
            minorTickEqualToNextMajorTickInd = np.nonzero(np.array(MinorTicks)==MajorTicks[majorCount+1])[0][0]
            toAppendIndices = range(minorTickEqualToThisMajorTickInd,minorTickEqualToNextMajorTickInd,NiceTimeJump)
            [ThinnedTickIndex.append(i) for i in toAppendIndices]
          else:
            # This is the last major tick, so fill in the remaining minor ticks.
            toAppendIndices = range(minorTickEqualToThisMajorTickInd,len(MinorTicks),NiceTimeJump)
            [ThinnedTickIndex.append(i) for i in toAppendIndices]
          majorCount = majorCount + 1
        # end for each major tick

        # Remove minor ticks placed too close to a major tick. Example when tlims are
        # [datetime.datetime(1999, 12, 31, 23, 59, 55), datetime.datetime(2000, 2, 24, 23, 59, 55)],
        # the March 31 and Feb 1 ticks are crammed together.
        ThinnedTickIndex = np.unique(np.array(ThinnedTickIndex))

        # ...Get index to points that are too close to the following point.
        if NiceTimeJump > 1:
            # Python 3 requires Boolean index to be the same length as the array
            # it is indexing, so pad with a False at the end (by definition,
            # the last element can't be too close to the following element).
            #IsTooCloseIndex = ThinnedTickIndex[np.diff(np.array(ThinnedTickIndex)) < NiceTimeJump-1]
            thisInd = np.diff(np.array(ThinnedTickIndex)) < NiceTimeJump-1
            thisInd = np.hstack((thisInd,False))
            IsTooCloseIndex = ThinnedTickIndex[thisInd]
        else:
            #IsTooCloseIndex = ThinnedTickIndex[np.diff(np.array(ThinnedTickIndex)) < NiceTimeJump]
            thisInd = np.diff(np.array(ThinnedTickIndex)) < NiceTimeJump
            thisInd = np.hstack((thisInd,False))
            IsTooCloseIndex = ThinnedTickIndex[thisInd]
        # end # if

        # ...Any points found are the point *preceding* too small a time gap.
        doRemoveInd = []
        for ind in IsTooCloseIndex:
          if np.array(MinorTicks)[ind] in np.array(MajorTicks):
            # This index points to a major tick, so don't remove it. Instead,
            # remove the minor tick that follows it.
            doRemoveInd.append(ind+1)
          else:
            # This index points to a minor tick, so remove it.
            doRemoveInd.append(ind)

        ThinnedTickIndex = np.setdiff1d(np.array(ThinnedTickIndex),doRemoveInd)

      else:
          # There are no major ticks.

          # No major point to anchor the choice of tick values, so try to find a tick value that
          # is divisible by the time increment between the thinned ticks (NiceTimeJump).
          if BaseUnits == timeUnitCodes['YEARS']:
              ValList = [MinorTicks[i].year for i in range(len(MinorTicks))]
          elif BaseUnits == timeUnitCodes['MONTHS']:
              ValList = [MinorTicks[i].month for i in range(len(MinorTicks))]
          elif BaseUnits == timeUnitCodes['DAYS']:
              ValList = [MinorTicks[i].day for i in range(len(MinorTicks))]
          elif BaseUnits == timeUnitCodes['HOURS']:
              ValList = [MinorTicks[i].hour for i in range(len(MinorTicks))]
          elif BaseUnits == timeUnitCodes['MINUTES']:
              ValList = [MinorTicks[i].minute for i in range(len(MinorTicks))]
          elif BaseUnits == timeUnitCodes['SECONDS']:
              ValList = [MinorTicks[i].second for i in range(len(MinorTicks))]
          elif BaseUnits == timeUnitCodes['MILLISECONDS']:
              ValList = [MinorTicks[i].microsecond/1000 for i in range(len(MinorTicks))]

          AnchorIndex = np.nonzero(np.array([np.remainder(ValList[i],NiceTimeJump) for i in range(len(ValList))]) == 0)
          AnchorIndex = AnchorIndex[0]

          if len(AnchorIndex) == 0:
            ThinnedTickIndex = range(0, len(ValList), NiceTimeJump)
          else:
            AnchorIndex = AnchorIndex[0]
            # Have to do range from AnchorIndex down to -1, not down to zero, because range does not include the endpoint in its output.
            #ThinnedTickIndex = [int(i) for i in np.union1d(np.flip(np.array(range(AnchorIndex,0,-NiceTimeJump)),0), range(AnchorIndex+NiceTimeJump, len(ValList), NiceTimeJump))]

            # Keep compatibility with python 2 by not using np.flip. Use np.fliplr instead; this requires reshaping
            # into 2-d object first...actually, don't need to flip at all, since union1d sorts results anyway.
            #ThinnedTickIndex = [int(i) for i in np.union1d(np.flip(np.array(range(AnchorIndex,-1,-NiceTimeJump)),0), range(AnchorIndex+NiceTimeJump, len(ValList), NiceTimeJump))]
            ThinnedTickIndex = [int(i) for i in np.union1d(np.array(range(AnchorIndex,-1,-NiceTimeJump)), range(AnchorIndex+NiceTimeJump, len(ValList), NiceTimeJump))]
          #end # if

      #end # if ~isempty(MajorTicks)
      tickVals = np.unique(np.array(MinorTicks)[ThinnedTickIndex])

  #end # if too many ticks.

  # 2018-05-25: was resulting in "DeprecationWarning: elementwise == comparison failed" warning.
  #if np.all(tickVals==MajorTicks):
  if np.array_equal(tickVals,MajorTicks):
     # The thinning of ticks has resulted in base tick values that coincide
     # exactly with all the major ticks. The axis labels should reflect this.
     axLabelUnits = MajorTicksUnits
  else:
     axLabelUnits = BaseUnits

  # Create the tick label strings.

  # Assemble list of labels for the thinned time axis ticks.
  tickTypes = []

  # For each tick...
  for TickCount in range(len(tickVals)):
      CurrTick = tickVals[TickCount]

      if len(MajorTicks)>0:
          IsMajorTick = CurrTick in MajorTicks
      else:
          IsMajorTick = False

      if IsMajorTick:
          if CurrTick in YearTicks:
              tickTypeCode = timeUnitCodes['YEARS']
          elif CurrTick in MonthTicks:
              tickTypeCode = timeUnitCodes['MONTHS']
          elif CurrTick in DayTicks:
              tickTypeCode = timeUnitCodes['DAYS']
          elif CurrTick in HourTicks or CurrTick in MinuteTicks:
              tickTypeCode = timeUnitCodes['MINUTES']
          elif CurrTick in SecondTicks:
              tickTypeCode = timeUnitCodes['SECONDS']
      else:
          # The current tick is not a major tick; give it a minor tick label.
          if BaseUnits == timeUnitCodes['YEARS']:
              tickTypeCode = timeUnitCodes['YEARS']
          elif BaseUnits == timeUnitCodes['MONTHS']:
              tickTypeCode = timeUnitCodes['MONTHS']
          elif BaseUnits == timeUnitCodes['DAYS']:
              tickTypeCode = timeUnitCodes['DAYS']
          elif BaseUnits == timeUnitCodes['HOURS'] or BaseUnits == timeUnitCodes['MINUTES']:
              tickTypeCode = timeUnitCodes['MINUTES']
          elif BaseUnits == timeUnitCodes['SECONDS']:
              tickTypeCode = timeUnitCodes['SECONDS']
          elif BaseUnits == timeUnitCodes['MILLISECONDS']:
              tickTypeCode = timeUnitCodes['MILLISECONDS']

      tickTypes.append(tickTypeCode)

  # Tick values must be converted to floats before they can be used on axes.
  tickVals = mdate.date2num(tickVals)
  tickTypes = dict(zip(tickVals,tickTypes))

  # Determine the information that generate_axlabel() will need to generate
  # the axis label string. The label should include only information not
  # included in the tick labels (e.g., the year if there are no year ticks on
  # the time axis). The choice of labels is complicated; for example, if the
  # minor ticks are months, the axis label should contain year information.
  # There should only be one year appearing in the axis label if:
  #   (a) there are no year ticks (i.e., all data lie in the same year);
  #   (b) there is one year tick, but that year tick is also the initial time
  #   limit (again, all data lie in the same year); or
  #   (c) there are two year ticks, but they correspond to the initial and
  #   final time limits (again, all data lie in the same year--apart from the
  #   final point, which may be thought of as the last moment of the first year).

  thisTicks = YearTicks
  numInLabel = {timeUnitCodes['YEARS']:1,timeUnitCodes['MONTHS']:0,timeUnitCodes['DAYS']:0,timeUnitCodes['SECONDS']:0}

  if len(thisTicks)==0 or (len(thisTicks)==1 and thisTicks[0] in tlims) or (len(thisTicks)==2 and np.all(thisTicks==tlims)):
     numInLabel[timeUnitCodes['YEARS']] = 1
  else:
     # Axis spans a range of years.
     numInLabel[timeUnitCodes['YEARS']] = 2

  if axLabelUnits == timeUnitCodes['YEARS']:
     numInLabel[timeUnitCodes['MONTHS']] = 0
     numInLabel[timeUnitCodes['DAYS']] = 0
     numInLabel[timeUnitCodes['SECONDS']] = 0
  else:
     thisTicks = MonthTicks
     if len(thisTicks)==0 or (len(thisTicks)==1 and thisTicks[0] in tlims) or (len(thisTicks)==2 and np.all(thisTicks==tlims)):
        numInLabel[timeUnitCodes['MONTHS']] = 1
     else:
        # Axis spans a range of months.
        numInLabel[timeUnitCodes['MONTHS']] = 2

     thisTicks = DayTicks
     if len(thisTicks)==0 or (len(thisTicks)==1 and thisTicks[0] in tlims) or (len(thisTicks)==2 and np.all(thisTicks==tlims)):
        numInLabel[timeUnitCodes['DAYS']] = 1
     else:
        # Axis spans a range of days.
        numInLabel[timeUnitCodes['DAYS']] = 2

     if axLabelUnits == timeUnitCodes['MILLISECONDS']:
        thisTicks = SecondTicks
        if len(thisTicks)==0 or (len(thisTicks)==1 and thisTicks[0] in tlims) or (len(thisTicks)==2 and np.all(thisTicks==tlims)):
           numInLabel[timeUnitCodes['SECONDS']] = 1
        else:
           # Axis spans a range of seconds.
           numInLabel[timeUnitCodes['SECONDS']] = 2
     else:
        # Seconds only appear in the label if plotting milliseconds.
        numInLabel[timeUnitCodes['SECONDS']] = 0

  return tickVals,tickTypes,axLabelUnits, numInLabel

#-----------------------------------------------------------------------
# If desired, over-ride the following code with your own custom tick-labelling
# and axis-labelling functions in a separate module. One or both of
# generate_ticklabel() and generate_axlabel() can be redefined in your
# custom module. For example, if you re-define generate_ticklabel() and/or
# generate_axlabel() in a module called "custom.py", call AdaptiveTimeScale
# with the syntax:
#   ax.set_xscale('adaptivetime',CustomModule="custom")
monthNums = [i for i in range(1,13)]
longMonthNames = dict(zip(monthNums,['January','February','March','April','May','June','July','August','September','October','November','December']))
shortMonthNames = dict(zip(monthNums,['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec']))

def generate_ticklabel(scaleObj,tickVal,tickTypes):
   timeUnitCodes = scaleObj.timeUnitCodes
   monthNames = shortMonthNames
   tickTimeVal = mdate.num2date(tickVal)
   monthName = monthNames[tickTimeVal.month]

   # 2018-08-02-Workaround for fact that strftime('%Y') fails for years < 1900. Extract a year string for
   # use if the tick type is years, then "correct" the year value (which is no longer needed
   # here) to a value >= 1900.
   yearStr = "%04d" % tickTimeVal.year
   if tickTimeVal.year < 1900:
      tickTimeVal = dt.datetime(1901,tickTimeVal.month,tickTimeVal.day,tickTimeVal.hour,tickTimeVal.minute,tickTimeVal.second,tickTimeVal.microsecond)
                      
   # Look in the scale objects "extraArgs" field for arguments passed down to
   # this function.
   #extraArgs = scaleObj.extraArgs

   # Reverse dictionary for development only.
   #rev = {v: k for k, v in timeUnitCodes.items()}

   # The smallest time tick value corresponds to the largest code value.
   smallestTickUnitCode = np.max(np.array(list(tickTypes.values())))

   if tickTypes[tickVal] == timeUnitCodes['YEARS']:
      #tickLabelStr = tickTimeVal.strftime('%Y')
      tickLabelStr = yearStr
   elif tickTypes[tickVal] == timeUnitCodes['MONTHS']:
       tickLabelStr = monthName
   elif tickTypes[tickVal] == timeUnitCodes['DAYS']:
       # Day ticks have different strings depending on whether they are "major"
       # or "minor" ticks.
       if tickTypes[tickVal] == smallestTickUnitCode:
           tickLabelStr = tickTimeVal.strftime('%d')
       else:
           tickLabelStr = monthName + " " + tickTimeVal.strftime('%d')
   elif tickTypes[tickVal] == timeUnitCodes['HOURS'] or tickTypes[tickVal] == timeUnitCodes['MINUTES']:
       # Hour and minute ticks look better with full HH:MM:SS format if the smallest ticks
       # are millisecond ticks.
       if smallestTickUnitCode == timeUnitCodes['MILLISECONDS']:
           tickLabelStr = tickTimeVal.strftime('%H:%M:%S')
       else:
           tickLabelStr = tickTimeVal.strftime('%H:%M')
   elif tickTypes[tickVal] ==  timeUnitCodes['SECONDS']:
       tickLabelStr = tickTimeVal.strftime('%H:%M:%S')
   elif tickTypes[tickVal] ==  timeUnitCodes['MILLISECONDS']:
       tickLabelStr = ("%.3f" % float((np.round(int(tickTimeVal.strftime('%f'))/1000.))/1000.))[1:]

   return tickLabelStr
   #return "A"

def generate_axlabel(scaleObj,tlims,axLabelUnits,numInLabel,timeZoneStr):

   # Look in the scale objects "extraArgs" field for arguments passed down to
   # this function.
   #extraArgs = scaleObj.extraArgs

   timeUnitCodes = scaleObj.timeUnitCodes

   monthNames = longMonthNames
   month1Name = monthNames[tlims[0].month]
   month2Name = monthNames[tlims[1].month]

   # 2018-08-02-Workaround for fact that strftime('%Y') fails for years < 1900. Extract a year string for
   # use if the tick type is years, then "correct" the year value (which is no longer needed
   # here) to a value >= 1900.
   #year1Str = tlims[0].strftime('%Y')
   #year2Str = tlims[1].strftime('%Y')
   year1Str = "%04d" % tlims[0].year
   year2Str = "%04d" % tlims[1].year

   correctedTlims = list(tlims) 
   if correctedTlims[0].year < 1900:
      correctedTlims[0] = dt.datetime(1901,correctedTlims[0].month,correctedTlims[0].day,correctedTlims[0].hour,correctedTlims[0].minute,correctedTlims[0].second,correctedTlims[0].microsecond)
      correctedTlims[1] = dt.datetime(1902,correctedTlims[1].month,correctedTlims[1].day,correctedTlims[1].hour,correctedTlims[1].minute,correctedTlims[1].second,correctedTlims[1].microsecond)
   
   day1Str = correctedTlims[0].strftime('%d')
   day2Str = correctedTlims[1].strftime('%d')
   ym1Str = month1Name + ' ' + year1Str
   ym2Str = month2Name + ' ' + year2Str
   ymd1Str = month1Name + ' ' + day1Str + ', ' + year1Str
   ymd2Str = month2Name + ' ' + day2Str + ', ' + year2Str
   md1Str = month1Name + ' ' + day1Str
   yd2Str = day2Str + ', ' + year2Str
   ymdhms1Str = ymd1Str + ' ' + correctedTlims[0].strftime('%H:%M:%S')

   # Will append a whitespace-delineated time zone string to the label, if one specified.
   if len(timeZoneStr)>0:
     timeZoneStr = ' ' + timeZoneStr

   numYearsInLabel = numInLabel[timeUnitCodes['YEARS']]
   numMonthsInLabel = numInLabel[timeUnitCodes['MONTHS']]
   numDaysInLabel = numInLabel[timeUnitCodes['DAYS']]
   numSecondsInLabel = numInLabel[timeUnitCodes['SECONDS']]

   # Reverse dictionary for development only.
   #rev = {v: k for k, v in timeUnitCodes.items()}

   if axLabelUnits == timeUnitCodes['YEARS']:
      axLabelStr = 'Time (years)' + timeZoneStr

   elif axLabelUnits == timeUnitCodes['MONTHS']:
      if numYearsInLabel == 1:
          axLabelStr = 'Time (months in ' + year1Str + timeZoneStr + ')'
      else:
          axLabelStr = 'Time (months in ' + year1Str + '-' + year2Str + timeZoneStr + ')'

   elif axLabelUnits == timeUnitCodes['DAYS']:
      if numMonthsInLabel == 1:
          axLabelStr = 'Time (days in ' + ym1Str + timeZoneStr + ')'
      elif numYearsInLabel == 1:
         axLabelStr = 'Time (days in ' + month1Name + '-' + ym2Str + timeZoneStr + ')'
      elif numYearsInLabel == 2:
         axLabelStr = 'Time (days in ' + ym1Str + '-' + ym2Str + timeZoneStr + ')'

   elif axLabelUnits in (timeUnitCodes['HOURS'],timeUnitCodes['MINUTES'],timeUnitCodes['SECONDS']):
      if numDaysInLabel == 1:
         axLabelStr = 'Time on ' + ymd1Str + timeZoneStr
      elif numMonthsInLabel == 1:
         axLabelStr = 'Time in ' + md1Str + '-' + yd2Str + timeZoneStr
      elif numYearsInLabel == 1:
         axLabelStr = 'Time in ' + md1Str + '-' + ymd2Str + timeZoneStr
      else:
         axLabelStr = 'Time in ' + ymd1Str + '-' + ymd2Str + timeZoneStr

   elif axLabelUnits ==  timeUnitCodes['MILLISECONDS']:
      if numSecondsInLabel == 1:
         axLabelStr = 'Time (fractional seconds since ' + ymdhms1Str + timeZoneStr + ')'
      elif numDaysInLabel == 1:
         axLabelStr = 'Time (fractional seconds in ' + ymd1Str + timeZoneStr + ')'
      elif numMonthsInLabel == 1:
         axLabelStr = 'Time (fractional seconds in ' + md1Str + '-' + yd2Str + timeZoneStr + ')'
      elif numYearsInLabel == 1:
         axLabelStr = 'Time (fractional seconds in ' + md1Str + '-' + ymd2Str + timeZoneStr + ')'
      else:
         axLabelStr = 'Time (fractional seconds in ' + ymd1Str + '-' + ymd2Str + timeZoneStr + ')'

   return axLabelStr
   #return "B"
#-----------------------------------------------------------------------

mscale.register_scale(AdaptiveTimeScale)

if __name__ == "__main__":
    # Call in demo mode.
    print("Calling AdaptiveTimeScale.py in demo mode...")
    import matplotlib.pyplot as plt

    y = np.array([440, 100, 200, 50, 400, 300, 60, 70, 80, 90, 100])
    timeDelta = dt.timedelta(hours=1)
    t0 = dt.datetime(2015, 12, 31, 16, 0, 0, 0)
    t = np.array([t0 + timeDelta * j for j in range(len(y))])
    tlims = [np.min(t),np.max(t)]
    #tlims = [dt.datetime(2100, 12, 30, 0, 0, 0, 0),dt.datetime(2205, 1, 1, 1, 0, 0, 0)]
    #tlims = [dt.datetime(100, 12, 30, 0, 0, 0, 0),dt.datetime(220, 1, 1, 1, 0, 0, 0)]
    #tlims = [dt.datetime(137, 9, 30, 23, 53, 0, 0),dt.datetime(137, 10, 1, 0, 2, 0, 0)]
    #tlims = [dt.datetime(9997, 12, 30, 0, 59, 58, 0),dt.datetime(9998, 12, 30, 23, 59, 59, 0)]
    #tlims = [dt.datetime(2005, 1, 1, 6, 39, 37, 0), dt.datetime(2005, 1, 11, 4, 15, 37, 0)]
    #tlims = [dt.datetime(2122, 6, 7, 16, 52, 16, 0), dt.datetime(2123, 3, 10, 20, 17, 49, 0)]
    print("tlims",tlims)

    fig = plt.figure()
    ax1 = fig.add_subplot(2,1,1)
    ax1.plot(t, y,'o-')
    ax1.set_xlim([tlims[0],tlims[1]])
    #ax1.set_xscale('adaptivetime',doLabelTicks=False,doLabelAx=False,CustomModule="my_custom_formats",bonusArg="a bonus argument",anotherBonusArg=99)
    ax1.set_xscale('adaptivetime',doLabelTicks=False,doLabelAx=False)

    ax1.set_ylabel('y')
    ax1.set_title('Adaptive Time Scale--Demo Mode')
    ax2 = fig.add_subplot(2,1,2)
    ax2.plot(t, 1000-y,'o-')
    ax2.set_xlim([tlims[0],tlims[1]])
    #ax2.set_xscale('adaptivetime',timeZoneStr="UTC",CustomModule="my_custom_formats")
    #ax2.set_xscale('adaptivetime',timeZoneStr="UTC",CustomModule="my_custom_formats_french")
    ax2.set_xscale('adaptivetime',timeZoneStr="UTC")
    ax2.set_ylabel('1000-y')
    plt.show()
