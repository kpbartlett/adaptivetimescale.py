For use in making time-series plots with pyplot.

Automatically adjusts time tick locations, time tick labels, and x-axis label to create compact, but completely unambiguous time-axis labelling.

Just as you would use:

  ax.set_xscale('log')
  
to set a plot's x-axis to a logarithmic scale, you can use:

  ax.set_xscale('adaptivetime')
  
to set a plot's x-axis to have adaptive time ticks and labels.

See https://kpbartlett.bitbucket.io/AdaptiveTimeScale.py for examples and description.

(Note: if you find the bitbucket user interface as confusing as I do, you'll be wondering how you can download the source code for this program. Click on the "Downloads" menu item on your left, then click on the "Download repository" link).

As a reminder to myself, this is the command to clone this repository:
  git clone https://bitbucket.org/kpbartlett/AdaptiveTimeScale.py/
