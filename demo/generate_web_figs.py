import sys
sys.path.append('/Users/kpb/home/cloud/forDropbox/onc/tasks/python_ata/AdaptiveTimeScale.py')

# Prepend this to all examples:
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
import matplotlib.dates as mdate

# Prepend this to all examples except the "complicated" example.
numPts = 50
startTime = dt.datetime(2005, 1, 1, 16, 0, 0, 0)
#startTime = dt.datetime(2004, 11, 28, 16, 0, 0, 0)
endTime = dt.datetime(2005, 1, 10, 16, 0, 0, 0)
x = [mdate.num2date(i) for i in np.linspace(mdate.date2num(startTime),mdate.date2num(endTime),numPts)]
y = [np.sin(i) for i in np.linspace(0,2*np.pi,numPts)]

# Prepend this to all examples using AdaptiveTimeScale:
import AdaptiveTimeScale

##############################################################
# Make plain-Jane autofmt_xdate plot.
fig, ax = plt.subplots() 
plt.plot(x,y,'o-')
fig.autofmt_xdate()
#plt.show()
fig.savefig('autofmt_wide.png',dpi=100)

##############################################################
# Zoomed-in autofmt.
fig, ax = plt.subplots() 
plt.plot(x,y,'o-')
xlims = [dt.datetime(2005, 1, 5, 6, 0, 0, 0), dt.datetime(2005, 1, 5, 20, 0, 0, 0)]
ax.set_xlim(xlims)
fig.autofmt_xdate()
#plt.show()
fig.savefig('autofmt_zoom.png',dpi=100)

##############################################################
# AdaptiveTimeScale wide
fig, ax = plt.subplots() 
plt.plot(x,y,'o-')
ax.set_xscale('adaptivetime')
#plt.show()
fig.savefig('ats_wide.png',dpi=100)

##############################################################
# AdaptiveTimeScale zoom
fig, ax = plt.subplots() 
plt.plot(x,y,'o-')
xlims = [dt.datetime(2005, 1, 5, 6, 0, 0, 0), dt.datetime(2005, 1, 5, 20, 0, 0, 0)]
ax.set_xlim(xlims)
ax.set_xscale('adaptivetime')
#plt.show()
fig.savefig('ats_zoom.png',dpi=100)

##############################################################
# AdaptiveTimeScale complicated
startTime = dt.datetime(2015, 12, 31, 16, 0)
#endTime = dt.datetime(2016, 1, 9, 16, 0)
endTime = dt.datetime(2016, 2, 3, 16, 0)
x = [mdate.num2date(i) for i in np.linspace(mdate.date2num(startTime),mdate.date2num(endTime),numPts)]
y = [np.sin(i) for i in np.linspace(0,2*np.pi,numPts)]
fig = plt.figure()
ax1 = fig.add_subplot(2,1,1)
ax1.plot(x, y,'o-')
#ax1.set_xlim([tlims[0],tlims[1]])
ax1.set_xscale('adaptivetime',doLabelTicks=False,doLabelAx=False,maxTicks=10)
ax1.set_ylabel('y')
ax2 = fig.add_subplot(2,1,2)
ax2.plot(x, y,'o-')
#ax2.set_xlim([tlims[0],tlims[1]])
ax2.set_xscale('adaptivetime',timeZoneStr="UTC",maxTicks=10)
ax2.set_ylabel('y')
#plt.show()
fig.savefig('ats_complicated_1.png',dpi=100)

##############################################################
# AdaptiveTimeScale french
fig, ax = plt.subplots() 
plt.plot(x,y,'o-')
ax.set_xscale('adaptivetime',CustomModule='my_custom_formats_french')
#plt.show()
fig.savefig('ats_wide_french.png',dpi=100)

########################################################################
# ATS, complicated 2
import AdaptiveTimeScale
import datetime as dt
import numpy as np
import matplotlib.pyplot as plt
y = np.array([440, 100, 200, 50, 400, 300, 60, 70, 80, 90])
timeDelta = dt.timedelta(days=1)
t0 = dt.datetime(2015, 12, 31, 16, 0, 0, 0)
t = [t0 + timeDelta * j for j in range(len(y))]
tlims = [np.min(t),np.max(t)]
fig = plt.figure()
ax1 = fig.add_subplot(2,1,1)
ax1.plot(t, y,'o-')
ax1.set_xlim([tlims[0],tlims[1]])
ax1.set_xscale('adaptivetime',doLabelTicks=False,doLabelAx=False,maxTicks=10)
ax1.set_ylabel('y')
ax2 = fig.add_subplot(2,1,2)
ax2.plot(t, y,'o-')
ax2.set_xlim([tlims[0],tlims[1]])
ax2.set_xscale('adaptivetime',timeZoneStr="UTC",maxTicks=10)
ax2.set_ylabel('y')
#plt.show()
fig.savefig('ats_complicated_2.png',dpi=100)
