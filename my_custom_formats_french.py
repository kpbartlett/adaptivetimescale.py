#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.dates as mdate

monthNums = [i for i in range(1,13)]
longMonthNames = dict(zip(monthNums,['janvier','février','mars','avril','mai','juin','juillet','aout','septembre','octobre','novembre','décembre']))
shortMonthNames = dict(zip(monthNums,['janv.','févr.','mars','avril','mai','juin','juil.','aout','sept.','oct.','nov.','déc.']))

def generate_ticklabel(scaleObj,tickVal,tickTypes):
   timeUnitCodes = scaleObj.timeUnitCodes
   monthNames = shortMonthNames
   tickTimeVal = mdate.num2date(tickVal)
   monthName = monthNames[tickTimeVal.month]
   
   # Workaround for fact that strftime('%Y') fails for years < 1900:
   yearStr = "%04d" % tickTimeVal.year

   # Look in the scale objects "extraArgs" field for arguments passed down to
   # this function.
   #extraArgs = scaleObj.extraArgs

   # Reverse dictionary for development only.
   #rev = {v: k for k, v in timeUnitCodes.items()}

   # The smallest time tick value corresponds to the largest code value.
   smallestTickUnitCode = np.max(np.array(list(tickTypes.values())))

   if tickTypes[tickVal] == timeUnitCodes['YEARS']:
       #tickLabelStr = tickTimeVal.strftime('%Y')
       tickLabelStr = yearStr
   elif tickTypes[tickVal] == timeUnitCodes['MONTHS']:
       tickLabelStr = monthName
   elif tickTypes[tickVal] == timeUnitCodes['DAYS']:
       # Day ticks have different strings depending on whether they are "major"
       # or "minor" ticks.
       if tickTypes[tickVal] == smallestTickUnitCode:
           tickLabelStr = tickTimeVal.strftime('%d')
       else:
           tickLabelStr = monthName + " " + tickTimeVal.strftime('%d')
   elif tickTypes[tickVal] == timeUnitCodes['HOURS'] or tickTypes[tickVal] == timeUnitCodes['MINUTES']:
       # Hour and minute ticks look better with full HH:MM:SS format if the smallest ticks 
       # are millisecond ticks.
       if smallestTickUnitCode == timeUnitCodes['MILLISECONDS']:
           tickLabelStr = tickTimeVal.strftime('%H:%M:%S')
       else:
           tickLabelStr = tickTimeVal.strftime('%H:%M')
   elif tickTypes[tickVal] ==  timeUnitCodes['SECONDS']:
       tickLabelStr = tickTimeVal.strftime('%H:%M:%S')
   elif tickTypes[tickVal] ==  timeUnitCodes['MILLISECONDS']:
       tickLabelStr = ("%.3f" % float((np.round(int(tickTimeVal.strftime('%f'))/1000.))/1000.))[1:]
                  
   return tickLabelStr

def generate_axlabel(scaleObj,tlims,axLabelUnits,numInLabel,timeZoneStr):

   # Look in the scale objects "extraArgs" field for arguments passed down to
   # this function.
   #extraArgs = scaleObj.extraArgs

   timeUnitCodes = scaleObj.timeUnitCodes

   monthNames = longMonthNames
   month1Name = monthNames[tlims[0].month]
   month2Name = monthNames[tlims[1].month]
   # Workaround for fact that strftime('%Y') fails for years < 1900:
   #year1Str = tlims[0].strftime('%Y')
   #year2Str = tlims[1].strftime('%Y')
   year1Str = "%04d" % tlims[0].year
   year2Str = "%04d" % tlims[1].year

   day1Str = tlims[0].strftime('%d')
   day2Str = tlims[1].strftime('%d')
   ym1Str = month1Name + ' ' + year1Str
   ym2Str = month2Name + ' ' + year2Str
   ymd1Str = month1Name + ' ' + day1Str + ', ' + year1Str
   ymd2Str = month2Name + ' ' + day2Str + ', ' + year2Str
   md1Str = month1Name + ' ' + day1Str
   yd2Str = day2Str + ', ' + year2Str
   ymdhms1Str = ymd1Str + ' ' + tlims[0].strftime('%H:%M:%S')
   
   # Will append a whitespace-delineated time zone string to the label, if one specified.
   if len(timeZoneStr)>0:
     timeZoneStr = ' ' + timeZoneStr
 
   numYearsInLabel = numInLabel[timeUnitCodes['YEARS']]
   numMonthsInLabel = numInLabel[timeUnitCodes['MONTHS']]
   numDaysInLabel = numInLabel[timeUnitCodes['DAYS']]
   numSecondsInLabel = numInLabel[timeUnitCodes['SECONDS']]
    
   # Reverse dictionary for development only.
   #rev = {v: k for k, v in timeUnitCodes.items()}
   
   if axLabelUnits == timeUnitCodes['YEARS']:
      axLabelStr = 'Temps (années)' + timeZoneStr
   elif axLabelUnits == timeUnitCodes['MONTHS']:
      if numYearsInLabel == 1:
          axLabelStr = 'Temps (mois en ' + year1Str + timeZoneStr + ')'
      else:
          axLabelStr = 'Temps (mois en ' + year1Str + '-' + year2Str + timeZoneStr + ')'
          
   elif axLabelUnits == timeUnitCodes['DAYS']:
      if numMonthsInLabel == 1:
          axLabelStr = 'Temps (jours en ' + ym1Str + timeZoneStr + ')'
      elif numYearsInLabel == 1:
         axLabelStr = 'Temps (jours en ' + month1Name + '-' + ym2Str + timeZoneStr + ')'
      elif numYearsInLabel == 2:
         axLabelStr = 'Temps (jours en ' + ym1Str + '-' + ym2Str + timeZoneStr + ')'
         
   elif axLabelUnits in (timeUnitCodes['HOURS'],timeUnitCodes['MINUTES'],timeUnitCodes['SECONDS']):
      if numDaysInLabel == 1:
         axLabelStr = 'Temps en ' + ymd1Str + timeZoneStr
      elif numMonthsInLabel == 1:
         axLabelStr = 'Temps en ' + md1Str + '-' + yd2Str + timeZoneStr
      elif numYearsInLabel == 1:
         axLabelStr = 'Temps en ' + md1Str + '-' + ymd2Str + timeZoneStr
      else:
         axLabelStr = 'Temps en ' + ymd1Str + '-' + ymd2Str + timeZoneStr
         
   elif axLabelUnits ==  timeUnitCodes['MILLISECONDS']:
      if numSecondsInLabel == 1:
         axLabelStr = 'Temps (secondes fractionnaires depuis ' + ymdhms1Str + timeZoneStr + ')'
      elif numDaysInLabel == 1:
         axLabelStr = 'Temps (secondes fractionnaires en ' + ymd1Str + timeZoneStr + ')'
      elif numMonthsInLabel == 1:
         axLabelStr = 'Temps (secondes fractionnaires en ' + md1Str + '-' + yd2Str + timeZoneStr + ')'
      elif numYearsInLabel == 1:
         axLabelStr = 'Temps (secondes fractionnaires en ' + md1Str + '-' + ymd2Str + timeZoneStr + ')'
      else:
         axLabelStr = 'Temps (secondes fractionnaires en ' + ymd1Str + '-' + ymd2Str + timeZoneStr + ')'
                  
   return axLabelStr
